package nl.utwente.di.celciusToFarenheit;

import java.util.HashMap;

public class Converter {

    public double convert(String value) {
        double celcius = Double.parseDouble(value);
        return celcius * 1.8 + 32;
    }
}
